/**
 * DbLoadManager.java - range of methods related to loading of data into the database.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package nl.rheaa.jtql.db;

import java.io.File;
import java.nio.charset.Charset;
import java.sql.SQLException;

import nl.rheaa.jtql.cli.Option;
import nl.rheaa.jtql.constants.Constants;
import nl.rheaa.jtql.utils.FileUtils;

import org.apache.log4j.Logger;
import org.h2.tools.DeleteDbFiles;

/**
 * Class which is responsible for reading the specified CSV file and loading the
 * data into an H2 database.
 *
 * @author ajay
 *
 */

public class DbLoadManager extends AbstractDBManager {

    private static Logger LOG = Logger.getLogger(DbLoadManager.class);

    @Override
    public void execute() {
        importData();
    }

    /**
     * Method that checks whether the file is a valid object. If the conditions
     * specified in the method are not satisfied, then the file is not a legal
     * file.
     *
     * @param filePath
     *            the absolute path to the file.
     * @return true if the file is an illegal file, false otherwise.
     */
    private boolean isIllegalFile(String filePath) {
        final File file = new File(filePath);
        System.out.println("File : " + file.getAbsolutePath());
        return (file == null || !file.exists() || !file.canRead() || !file
                .isFile());
    }

    /**
     * Method that checks whether the file-path parameter is a legal one. If the
     * file-path provided is legal, then it initiates the data loading process.
     */
    private void importData() {
        final String filePath = Option.get(Constants.FILEPATH);
        LOG.debug("filePath : " + filePath);
        if (!FileUtils.baseNameExists(filePath) || isIllegalFile(filePath)) {
            throw new RuntimeException(
                    "Illegal file specified. File not found or doesn't have read permissions");
        } else {
            LOG.debug("Loading data ... ");
            try {
                loadData();
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method that actually loads the data into the H2 database.
     *
     * @param filePath
     *            the path to the CSV file.
     * @param separator
     *            the separator to be used while loading the data.
     * @throws SQLException
     */
    private void loadData() throws SQLException {
        DeleteDbFiles.execute(TEMP_DIRECTORY, DB_NAME, true);
        LOG.debug("Deleted the test db already created ... ");
        loadDbThings();
        final String csvReadStmt = generateQuery();
        try {
            statement.execute("create table "
                    + Option.get(Constants.DBTABLENAME) + " as " + csvReadStmt);
            LOG.info("Created the table and loaded the csv data into the database.");
            new DbViewManager().execute();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

    private String generateQuery() {
        final String filePath = Option.get(Constants.FILEPATH);
        final Charset charSet = Charset.forName(Option.get(Constants.ENCODING));
        final String separator = Option.get(Constants.SEPARATOR);
        final String delimiter = Option.get(Constants.QUOTE);
        final String escape = Option.get(Constants.ESCAPE);
        final String lineending = Option.get(Constants.LINEENDING);
        final String csvReadStmt = "select * from csvread (" + "'" + filePath
                + "'" + ", " + null + ", " + "'" + charSet + "'" + ", " + "'"
                + separator + "'" + "," + "'" + delimiter + "'" + "," + "'"
                + escape + "'" + "," + "'" + lineending + "'" + ")";
        LOG.info("query statement created: " + csvReadStmt);
        return csvReadStmt;
    }
}
