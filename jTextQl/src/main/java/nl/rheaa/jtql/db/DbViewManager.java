/**
 * DbViewManager.java - contains related methods for job related utilities.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package nl.rheaa.jtql.db;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nl.rheaa.jtql.cli.Option;
import nl.rheaa.jtql.constants.Constants;

import org.apache.log4j.Logger;
import org.h2.tools.Shell;

/**
 * This class contains methods which related to viewing and querying the data
 * imported to the database by the loader.
 * 
 * @author ajay
 */

public class DbViewManager extends AbstractDBManager {

    private static Logger LOG = Logger.getLogger(DbViewManager.class);
    private static final String H2_SCRIPT = "h2.sh";
    private static final String TEMP_DIRECTORY = "/tmp";
    private static final String USER_DIR = System.getProperty("user.dir");

    @Override
    public void execute() {
        final boolean browseDbOption = Option.get(Constants.BROWSER)
                .equalsIgnoreCase("yes");
        if (browseDbOption) {
            startBrowser();
        } else {
            startShell();
        }
    }

    /**
     * Method to start the browser.
     */
    private void startBrowser() {
        final List<String> commands = new ArrayList<String>();
        // TODO: make it applicable for all shells
        commands.add("/bin/bash");
        // Add arguments

        commands.add(USER_DIR + File.separatorChar + H2_SCRIPT);
        System.out.println(commands);
        final ProcessBuilder pb = new ProcessBuilder(commands);
        pb.directory(new File(TEMP_DIRECTORY));
        pb.redirectErrorStream(true);
        Process process = null;
        try {
            process = pb.start();
        } catch (final IOException e2) {
            e2.printStackTrace();
        }
        // Read output
        final StringBuilder out = new StringBuilder();
        final BufferedReader br = new BufferedReader(new InputStreamReader(
                process.getInputStream()));
        String line = null, previous = null;
        try {
            while ((line = br.readLine()) != null)
                if (!line.equals(previous)) {
                    previous = line;
                    out.append(line).append('\n');
                    System.out.println(line);
                }
        } catch (final IOException e1) {
            e1.printStackTrace();
        }
        // TODO: kill the process properly
        // Check result
        try {
            if (process.waitFor() == 0) {
                System.out.println("Success!");
            }
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
        System.exit(0);

        // Abnormal termination: Log command parameters and output and throw
        // ExecutionException
        System.err.println(commands);
        System.err.println(out.toString());
        System.exit(1);
        closeConnection();
        LOG.info("Connection closed...");
    }

    /**
     * Method to start the H2 shell.
     */
    private void startShell() {
        LOG.info("Starting shell ..");
        try {
            new Shell().runTool("-url", super.url, "-user", super.user,
                    "-password", super.password);
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }

}
