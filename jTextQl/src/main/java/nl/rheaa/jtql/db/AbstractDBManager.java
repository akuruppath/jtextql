/**
 * AbstractQueryJob.java - consists of methods that initializes database related stuff.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package nl.rheaa.jtql.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

/**
 * This class is the superclass for Job related classes. This class is primarily
 * responsible for holding the objects related to database connectivity.
 * 
 * @author ajay
 * 
 */

public abstract class AbstractDBManager {

    private static Logger LOG = Logger.getLogger(AbstractDBManager.class);
    protected static final String TEMP_DIRECTORY = System
            .getProperty("java.io.tmpdir");
    protected static final String FILE_SEPARATOR = System
            .getProperty("file.separator");
    protected static String driver = "org.h2.Driver";
    protected static String user = "sa";
    protected static String password = "";
    protected static String DB_NAME = "test";
    protected static String url = "jdbc:h2:" + TEMP_DIRECTORY + FILE_SEPARATOR
            + DB_NAME;
    protected Connection connection;
    protected Statement statement;

    /**
     * Constructor.
     */
    protected AbstractDBManager() {

    }

    /**
     * Method to close the database connection.
     */
    protected void closeConnection() {
        if (connection != null) {
            try {
                LOG.info("Closing the connection now...");
                connection.close();
            } catch (final SQLException e) {
                LOG.error(
                        "An exception happened while closing the connection.",
                        e);
            }
        }
    }

    /**
     * Method to get the Resultset object corresponding to a query.
     * 
     * @param query
     * @return resultset the resultset object corresponding to the query
     */
    protected ResultSet getResultSet(String query) {
        ResultSet rs = null;
        if (statement != null) {
            try {
                rs = statement.executeQuery(query);
            } catch (final SQLException e) {
                e.printStackTrace();
            }
        } else {
            throw new RuntimeException("Null statement object.");
        }
        return rs;
    }

    /**
     * Method that is responsible for loading the stuff necessary to connect to
     * the database. Initialises the driver, connection and the statement.
     */
    protected void loadDbThings() {
        try {
            Class.forName(driver);
            LOG.debug("Loaded the jdbc driver.");
            connection = DriverManager.getConnection(url, user, password);
            LOG.debug("Got connection object : " + connection);
            if (connection != null) {
                connection.setAutoCommit(true);
                statement = connection.createStatement();
            } else {
                LOG.error("Couldn't get connection object.");
            }
        } catch (final ClassNotFoundException e) {
            System.err
                    .println("Driver not found !! Check logs for the full stacktrace.");
            LOG.fatal("Driver not found !! ", e);
        } catch (final SQLException e) {
            LOG.fatal("Error connecting to the database !! ", e);
        }
    }

    public abstract void execute();

}
