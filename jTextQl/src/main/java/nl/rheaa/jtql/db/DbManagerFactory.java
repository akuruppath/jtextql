/**
 * DbManagerFactory.java - contains related methods for database related utilities.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package nl.rheaa.jtql.db;

import nl.rheaa.jtql.constants.Constants;

/**
 * @author ajay
 * 
 */
public class DbManagerFactory {

    /**
     * Factory to get the appropriate type of manager. If the argument type is
     * load, then it returns a dbloadmanager. If the argument type is view, then
     * it returns the dbviewmanager.
     * 
     * @param managerType
     * @return a type of AbstractDbManager
     */
    public AbstractDBManager getDataManager(String managerType) {
        AbstractDBManager manager = null;
        if (managerType.equals(Constants.LOAD)) {
            manager = new DbLoadManager();
        } else if (managerType.equals(Constants.VIEW)) {
            manager = new DbViewManager();
        }
        return manager;
    }
}
