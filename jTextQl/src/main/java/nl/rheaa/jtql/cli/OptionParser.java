/**
 * OptionParser.java - consists of range of methods that parses the CLI options.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package nl.rheaa.jtql.cli;

import nl.rheaa.jtql.constants.Constants;
import nl.rheaa.jtql.db.AbstractDBManager;
import nl.rheaa.jtql.db.DbManagerFactory;
import nl.rheaa.jtql.utils.FileUtils;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;

/**
 * This class consists of methods that are responsible for parsing the command
 * line options and check if they are legal or not.
 *
 * @author ajay
 *
 */

public class OptionParser {

    private static final CommandLineParser parser = new GnuParser();
    private static final Options jtqlOptions = new JtqlOptions()
            .getJtqlOptions();

    private static final String DEFAULT_CHARSET = "UTF-8";
    private static final String DEFAULT_DB_TABLENAME = "test";
    private static final String DEFAULT_ESCAPE = "/";
    private static final String DEFAULT_LINE_ENDING = "\n"; // unix line-ending
    private static final String DEFAULT_SEPARATOR = ",";
    private static final String DEFAULT_QUOTE = "double";

    private static final String DOUBLE_QUOTE = "\"";
    private static final String SINGLE_QUOTE = "'";
    private static final String WINDOWS_LINE_ENDING = "\r\n";

    /**
     * Main method of the program.
     *
     * @param args
     *            the arguments supplied from the command line.
     */
    public static void main(String[] args) {
        OptionParser.parseOptions(args);
        // load data
        final AbstractDBManager loadManager = new DbManagerFactory()
                .getDataManager(Constants.LOAD);
        loadManager.execute();
        // view data
        final AbstractDBManager viewManager = new DbManagerFactory()
                .getDataManager(Constants.VIEW);
        viewManager.execute();
    }

    /**
     * Method to parse the command-line arguments. Depending on the arguments
     * passed at the command line, it does different actions.
     *
     * @params args the arguments supplied from the command-line.
     *
     */
    private static void parseOptions(String[] args) {
        if (args.length <= 0) {
            printHelp();
        }
        try {
            final CommandLine line = parser.parse(jtqlOptions, args);
            if (line.hasOption(Constants.HELP)) {
                printHelp();
            }
            if (line.hasOption(Constants.VERSION)) {
                System.err.println(CliConstants.VERSION_MSG);
                System.exit(0);
            }
            // file-path value has to be parsed first.
            getFilePath(line);
            getBrowser(line);
            getDbTableName(line);
            getEscapeChar(line);
            getEncoding(line);
            getLineEndingChar(line);
            getQuoteChar(line);
            getSeparator(line);
        } catch (final UnrecognizedOptionException e) {
            throw new IllegalArgumentException(
                    "Error recognizing an argument: " + e.getMessage(), e);
        } catch (final ParseException e) {
            throw new IllegalArgumentException("Error parsing an argument : "
                    + e.getMessage(), e);
        }
    }

    private static void getBrowser(final CommandLine line) {
        final String browser = line.getOptionValue(Constants.BROWSER);
        extractAndSetOption(browser, Constants.BROWSER, "no");
    }

    private static void getDbTableName(final CommandLine line) {
        final String dbTableName = line.getOptionValue(Constants.DBTABLENAME);
        extractAndSetOption(dbTableName, Constants.DBTABLENAME,
                DEFAULT_DB_TABLENAME);
    }

    private static void getEscapeChar(CommandLine line) {
        final String escape = line.getOptionValue(Constants.ESCAPE);
        extractAndSetOption(escape, Constants.ESCAPE, DEFAULT_ESCAPE);
    }

    private static void getEncoding(final CommandLine line) {
        final String encoding = line.getOptionValue(Constants.ENCODING);
        extractAndSetOption(encoding, Constants.ENCODING, DEFAULT_CHARSET);
    }

    private static void getFilePath(final CommandLine line) {
        final String filePath = line.getOptionValue(Constants.FILEPATH);
        if (filePath == null || filePath.isEmpty()) {
            throw new IllegalArgumentException(CliConstants.MSG);
        } else {
            Option.set(Constants.FILEPATH, filePath);
        }
    }

    private static void getLineEndingChar(CommandLine line) {
        final String lineending = line.getOptionValue(Constants.LINEENDING);
        extractAndSetOption(lineending, Constants.LINEENDING,
                DEFAULT_LINE_ENDING);

    }

    private static void getQuoteChar(CommandLine line) {
        final String quote = line.getOptionValue(Constants.QUOTE);
        extractAndSetOption(quote, Constants.QUOTE, DOUBLE_QUOTE);
    }

    private static void getSeparator(final CommandLine line) {
        final String separator = line.getOptionValue(Constants.SEPARATOR);
        extractAndSetOption(separator, Constants.SEPARATOR, DEFAULT_SEPARATOR);
    }

    /**
     * Method that prints the help message, if no arguments were specified at
     * the command-line.
     *
     */
    private static void printHelp() {
        final HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(CliConstants.PROGRAM_NAME, jtqlOptions);
        System.exit(0);
    }

    /**
     * Method to set options.
     *
     * @param extractedOption
     * @param key
     * @param defaultOption
     */
    private static void extractAndSetOption(String extractedOption, String key,
            String defaultOption) {
        if (extractedOption == null) {
            Option.set(key, defaultOption);
            return;
        } else if (extractedOption.isEmpty()) {
            throw new IllegalArgumentException(CliConstants.MSG);
        } else if (key.equals(Constants.BROWSER)) {
            final String value = extractedOption.equalsIgnoreCase("yes") ? "yes"
                    : defaultOption;
            Option.set(key, value);
        } else if (key.equals(Constants.DBTABLENAME)) {
            final String value = extractedOption.equalsIgnoreCase("auto") ? FileUtils
                    .getFileName() : defaultOption;
            Option.set(key, value);
        } else if (key.equals(Constants.ESCAPE)) {
            final String value = extractedOption.equals(DEFAULT_ESCAPE) ? defaultOption
                    : extractedOption;
            Option.set(key, value);
        } else if (key.equals(Constants.ENCODING)) {
            final String value = extractedOption
                    .equalsIgnoreCase(DEFAULT_CHARSET) ? defaultOption
                    : extractedOption;
            Option.set(key, value);
        } else if (key.equals(Constants.LINEENDING)) {
            final String value = extractedOption.equals(DEFAULT_LINE_ENDING) ? defaultOption
                    : WINDOWS_LINE_ENDING;
            Option.set(key, value);
        } else if (key.equals(Constants.QUOTE)) {
            final String value = extractedOption.equals(DEFAULT_QUOTE) ? defaultOption
                    : SINGLE_QUOTE;
            Option.set(key, value);
        } else {
            Option.set(key, extractedOption);
        }
    }
}
