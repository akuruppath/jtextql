/**
 * Option.java - list of functions to set and get command line options.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package nl.rheaa.jtql.cli;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * @author ajay
 * 
 */
public class Option {

    private static Logger LOG = Logger.getLogger(Option.class);
    private static final Map<String, String> optionMap = new HashMap<String, String>();

    /**
     * Method to retrieve an option from the option-map. If an unknown option or
     * a null is passed then it exits the program.
     * 
     * @param key
     *            that represents the option being searched for
     * @return String the value that corresponds to the CLI key that was sent as
     *         the argument.
     */
    public static String get(String key) {
        LOG.debug("Received key : " + key);
        if (key == null || optionMap.get(key) == null
                || !optionMap.containsKey(key)) {
            LOG.warn("Unknown key recieved : " + key
                    + " Was this option set ???");
            return null;
        }
        return optionMap.get(key);
    }

    static void set(String key, String value) {
        optionMap.put(key, value);
        LOG.debug("Added key:value pair " + key + ":" + value);
    }

}
