/**
 * JtqlOptions.java - list of functions to parse and use command line arguments.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package nl.rheaa.jtql.cli;

import nl.rheaa.jtql.constants.Constants;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.log4j.Logger;

/**
 * Class that contains methods responsible for : <code>
 * - creating command-line options.
 * - initialise the command-line options.
 * - parsing the command-line arguments.
 * </code>
 * 
 * @author ajay
 * 
 */

public final class JtqlOptions {

    private static Logger LOG = Logger.getLogger(JtqlOptions.class);

    private static Options jtqlOptions;

    static {
        initializeOptions();
    }

    /**
     * Method to return the options.
     * 
     * @return jdbReportOptions which contains all the options available for
     *         jdbReport.
     */
    Options getJtqlOptions() {
        return jtqlOptions;
    }

    /**
     * Method to create the options required for the program and initializes the
     * same.
     */
    private static void initializeOptions() {
        final Option helpOption = new Option(Constants.HELP,
                CliConstants.OPT_DESC_HELP);
        final Option versionOption = new Option(Constants.VERSION,
                CliConstants.OPT_DESC_VERSION);
        @SuppressWarnings("static-access")
        final Option separatorOption = OptionBuilder
                .withArgName(Constants.SEPARATOR).hasArgs(1).isRequired(false)
                .withDescription(CliConstants.OPT_DESC_SEPARATOR)
                .create(Constants.SEPARATOR);
        @SuppressWarnings("static-access")
        final Option filePathOption = OptionBuilder
                .withArgName(Constants.FILEPATH).hasArgs(1).isRequired(false)
                .withDescription(CliConstants.OPT_DESC_FILE_PATH)
                .create(Constants.FILEPATH);
        @SuppressWarnings("static-access")
        final Option browserOption = OptionBuilder
                .withArgName(Constants.BROWSER).hasArgs(1).isRequired(false)
                .withDescription(CliConstants.OPT_DESC_BROWSER)
                .create(Constants.BROWSER);
        @SuppressWarnings("static-access")
        final Option dbTableNameOption = OptionBuilder
                .withArgName(Constants.DBTABLENAME).hasArgs(1)
                .isRequired(false)
                .withDescription(CliConstants.OPT_DESC_TABL_NAME)
                .create(Constants.DBTABLENAME);
        @SuppressWarnings("static-access")
        final Option encodingOption = OptionBuilder
                .withArgName(Constants.ENCODING).hasArgs(1).isRequired(false)
                .withDescription(CliConstants.OPT_DESC_ENCODING)
                .create(Constants.ENCODING);
        @SuppressWarnings("static-access")
        final Option quoteOption = OptionBuilder.withArgName(Constants.QUOTE)
                .hasArgs(1).isRequired(false)
                .withDescription(CliConstants.OPT_DESC_QUOTE)
                .create(Constants.QUOTE);
        @SuppressWarnings("static-access")
        final Option lineEndingOption = OptionBuilder
                .withArgName(Constants.LINEENDING).hasArgs(1).isRequired(false)
                .withDescription(CliConstants.OPT_DESC_LINE_ENDING)
                .create(Constants.LINEENDING);
        @SuppressWarnings("static-access")
        final Option escapeOption = OptionBuilder.withArgName(Constants.ESCAPE)
                .hasArgs(1).isRequired(false)
                .withDescription(CliConstants.OPT_DESC_ESCAPE)
                .create(Constants.ESCAPE);
        jtqlOptions = new Options();
        jtqlOptions.addOption(helpOption);
        jtqlOptions.addOption(versionOption);
        jtqlOptions.addOption(separatorOption);
        jtqlOptions.addOption(quoteOption);
        jtqlOptions.addOption(lineEndingOption);
        jtqlOptions.addOption(escapeOption);
        jtqlOptions.addOption(filePathOption);
        jtqlOptions.addOption(browserOption);
        jtqlOptions.addOption(dbTableNameOption);
        jtqlOptions.addOption(encodingOption);
        LOG.info("Initialized options successfully.");
    }

}
