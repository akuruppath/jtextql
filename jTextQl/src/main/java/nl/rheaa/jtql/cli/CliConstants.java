/**
 * CliConstants.java - list of all constants related to the command line options.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package nl.rheaa.jtql.cli;

/**
 * This class contains a list of all the constants that are related to the
 * command line operations.
 *
 * @author ajay
 *
 */

public class CliConstants {

    public static final String OPT_DESC_BROWSER = "Starts a browser after importing of the data. \n"
            + " If a no is specified or this option is not specified at all, \n"
            + " the default option of shell will be started.\n"
            + " example usage : --browser=yes."
            + "\n Available options are yes/no.";

    public static final String OPT_DESC_ENCODING = "Encoding to be used while exporting the CSV file. The default value is UTF-8";

    public static final String OPT_DESC_ESCAPE = "The escape character used in the CSV file.";

    public static final String OPT_DESC_FILE_PATH = "The path to the csv file.";

    public static final String OPT_DESC_HELP = "Print this message and exit.";

    public static final String OPT_DESC_LINE_ENDING = "The line ending char used in the CSV file. The default value is unix. \n"
            + "example usage: --lineending=unix \n"
            + "Available options are unix/windows";

    public static final String OPT_DESC_QUOTE = "The quotation character used in the CSV file. Default value : double \n"
            + "example usage: --quote=single \n"
            + "Available options are double/single";

    public static final String OPT_DESC_SEPARATOR = "The separator character for the CSV file. Default value: \",\".\n"
            + "example usage: --separator=\";\"";

    public static final String OPT_DESC_TABL_NAME = "Auto-generates the database table name. The generated name will be the same as the name of\n"
            + " the CSV file being imported."
            + " example usage: --tablename=auto\n"
            + " Available option is auto";

    public static final String OPT_DESC_VERSION = "Print the version information and exit.";

    public static final String MSG = "Please specify a valid value.";

    public static final String PROGRAM_NAME = " jTextQL";

    public static final String PROGRAM_LICENSE = " \n"
            + " Copyright (C) 2014  Ajay Kuruppath\n\n"
            +

            " This library is free software; you can redistribute it and/or"
            + "\n"
            + " modify it under the terms of the GNU Lesser General Public"
            + "\n"
            + " License as published by the Free Software Foundation; either"
            + "\n"
            + " version 2.1 of the License, or (at your option) any later version."
            + "\n\n"
            +

            " This library is distributed in the hope that it will be useful,"
            + "\n"
            + " but WITHOUT ANY WARRANTY; without even the implied warranty of"
            + "\n"
            + " MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU"
            + "\n"
            + " Lesser General Public License for more details."
            + "\n\n"
            +

            " You should have received a copy of the GNU Lesser General Public"
            + "\n"
            + " License along with this library; if not, write to the Free Software"
            + "\n"
            + " Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA"
            + "\n";

    public static final String VERSION_MSG = "\n" + PROGRAM_NAME + " v1.0\n"
            + PROGRAM_LICENSE;

}
