/**
 * FileUtils.java - contains file related methods.
 *
 * Copyright (C) 2014  Ajay Kuruppath

 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
package nl.rheaa.jtql.utils;

import java.io.File;

import nl.rheaa.jtql.cli.Option;
import nl.rheaa.jtql.constants.Constants;

/**
 * This class consists of utilities related to files.
 * 
 * @author ajay
 * 
 */

public class FileUtils {

    /**
     * Method to check if a base name for a file exists.
     * 
     * @param filePath
     *            the absolute path to the file.
     * @return true if a base-name for the CSV file exists else false.
     */
    public static boolean baseNameExists(String filePath) {
        // use zero-width positive lookahead:
        // http://stackoverflow.com/questions/4545937/java-splitting-the-filename-into-a-base-and-extension
        boolean baseNameExists = false;
        final File f = new File(filePath);
        final String[] baseNameTokens = f.getName().split("\\.(?=[^\\.]+$)");
        if (baseNameTokens.length > 1) {
            baseNameExists = baseNameTokens[1].equalsIgnoreCase("csv");
        }
        return baseNameExists;
    }

    /**
     * Method to extract the name of the CSV file being imported,
     * 
     * @return String string that represents the name of the CSV file being
     *         exported.
     */
    public static String getFileName() {
        String fileName = null;
        final String filePath = Option.get(Constants.FILEPATH);
        if (baseNameExists(filePath)) {
            final File f = new File(filePath);
            final String baseName = f.getName();
            fileName = baseName.substring(0, baseName.lastIndexOf("."));
        }
        return fileName;
    }
}
